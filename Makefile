# Makefile for xdesktopwaves

#===================== Configuration for inline assembler ======================

# This should be adapted if you get errors from the assembler: Which level of
# instruction set is supported by the x86 assembler behind gcc? 2 means MMX and
# SSE2, 1 means just MMX and no SSE2, and 0 means no MMX and no SSE2. (support
# by the CPU is checked at run-time)

XDW_MAX_OPTIMIZATION=2

#=========================== C compiler configuration ==========================

CC     = gcc
CFLAGS = -I/usr/X11R6/include -O2

#============================= Linker configuration ============================

LINK   = gcc
LFLAGS = -L/usr/X11R6/lib
LIBS   = -lm -lX11 -lXext

#============================ Installer configuration ==========================

BINDIR  = /usr/X11R6/bin
MAN1DIR = /usr/X11R6/man/man1
INSTALL = install
RM      = rm -f -v

#===============================================================================

all: xdesktopwaves

xdesktopwaves.o: xdesktopwaves.c
	$(CC) $(CFLAGS) -DXDW_MAX_OPTIMIZATION=$(XDW_MAX_OPTIMIZATION) -c xdesktopwaves.c -o xdesktopwaves.o

xdesktopwaves: xdesktopwaves.o
	$(LINK) $(LFLAGS) xdesktopwaves.o $(LIBS) -o xdesktopwaves

clean:
	$(RM) xdesktopwaves.o

install: xdesktopwaves xdesktopwaves.1
	$(INSTALL) -c -m 0755 -s xdesktopwaves $(BINDIR)/xdesktopwaves
	$(INSTALL) -c -m 0444 xdesktopwaves.1 $(MAN1DIR)/xdesktopwaves.1

uninstall:
	$(RM) $(BINDIR)/xdesktopwaves
	$(RM) $(MAN1DIR)/xdesktopwaves.1

/*****************************************************************************
   xdwapidemo.h - demo program on the xdesktopwaves API

   Copyright (C) 2004 Oliver Hamann (olha@users.sourceforge.net)

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "xdwapi.h"


int main(int argc, char * argv[])
{
	Display * display;
	xdwApiContext context;
	int i, width, height, ghostId1, ghostId2, ghostId3, d;

	/* This is an example of using the xdesktopwaves API functions.
	   Even throw an eye on the comments in the header file "xdwapi.h". */

	printf("XOpenDisplay...\n");
	display=XOpenDisplay(NULL);
	if (!display) {
		fprintf(stderr,"%s: cannot open display %s\n",argv[0],XDisplayName(NULL));
		exit(1);
	}
	width=WidthOfScreen(DefaultScreenOfDisplay(display));
	height=HeightOfScreen(DefaultScreenOfDisplay(display));

	printf("xdwApiOpen...\n");
	context=xdwApiOpen(display);

	printf("xdwApiPing...\n");
	if (xdwApiPing(context)==False) {
		printf("=> warning: xdesktopwaves probably not running\n");
	}

	printf("xdwApiSetRain...\n");
	for (i=0; i<=10; i++) {
		xdwApiSetRain(context,i);
		usleep(700000);
	}
	xdwApiSetRain(context,0);

	printf("xdwApiSetStorm...\n");
	for (i=0; i<=10; i++) {
		xdwApiSetStorm(context,i);
		usleep(700000);
	}
	xdwApiSetStorm(context,0);

	printf("xdwApiClear...\n");
	xdwApiClear(context);
	usleep(1000000);

	printf("xdwApiPutRaindrop...\n");
	xdwApiPutRaindrop(context,width/4,height/4,50);
	usleep(500000);
	xdwApiPutRaindrop(context,width*3/4,height/4,50);
	usleep(500000);
	xdwApiPutRaindrop(context,width/4,height*3/4,50);
	usleep(500000);
	xdwApiPutRaindrop(context,width*3/4,height*3/4,50);
	usleep(1000000);
	xdwApiPutRaindrop(context,width/2,height/2,100);
	usleep(1000000);

	printf("xdwApiPutGhostCircle...\n");
	for (i=0; i<320; i++) {
		xdwApiPutGhostCircle(
			context,
			300,
			0,
			width/2+sin(i*0.04)*width/3,
			height/2-cos(i*0.04)*height/3,
			10
		);
		usleep(20000);
	}

	printf("xdwApiPutGhostCircle (with substitution)...\n");
	ghostId1=0;
	for (i=0; i<320; i++) {
		ghostId1=xdwApiPutGhostCircle(
			context,
			300,
			ghostId1,
			width/2-sin(i*0.04)*width/3,
			height/2-cos(i*0.04)*height/3,
			10
		);
		usleep(20000);
	}

	printf("xdwApiPutGhostCircle (with substitution, three ghosts)...\n");
	ghostId1=0;
	ghostId2=0;
	ghostId3=0;
	for (i=0; i<480; i++) {
		ghostId1=xdwApiPutGhostCircle(
			context,
			300,
			ghostId1,
			width/2-sin(i*0.04)*width/3,
			height/2-cos(i*0.04)*height/3,
			8+i*i/2000
		);
		ghostId2=xdwApiPutGhostCircle(
			context,
			300,
			ghostId2,
			width/2-sin(i*0.065)*(width/3+sin(i*0.1)*width/7),
			height/2-cos(i*0.065)*(height/3+sin(i*0.1)*height/7),
			8+i*i/2000
		);
		ghostId3=xdwApiPutGhostCircle(
			context,
			300,
			ghostId3,
			width/2-sin(i*0.015)*sin(i*0.02)*sin(i*0.025)*width/2,
			height/2-cos(i*0.015)*cos(i*0.02)*cos(i*0.025)*height/2,
			8+i*i/2000
		);
		usleep(20000);
	}

	printf("xdwApiPutGhostRect...\n");
	for (i=0; i<200; i++) {
		xdwApiPutGhostRect(
			context,
			300,
			0,
			i*width/200,
			height/4,
			40,
			height/2
		);
		usleep(20000);
	}

	printf("xdwApiPutGhostRect (with substitution)...\n");
	ghostId1=0;
	for (i=0; i<480; i++) {
		d=sin(i*(0.1+i/200))*2000/(i+20);
		ghostId1=xdwApiPutGhostRect(
			context,
			300,
			ghostId1,
			width/2-60-d,
			height/2-60-d,
			120+d*2,
			120+d*2
		);
		usleep(20000);
	}

	printf("xdwApiClose...\n");
	xdwApiClose(context);

	printf("XCloseDisplay...\n");
	XCloseDisplay(display);

	return 0;
}
